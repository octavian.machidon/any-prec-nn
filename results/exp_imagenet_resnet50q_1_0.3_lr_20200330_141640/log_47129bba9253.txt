2020-03-30 14:16:42 - INFO - running arguments: Namespace(batch_size=256, bit_width_list='1,2,4,8,32', dataset='imagenet', epochs=80, lr=0.5, lr_decay='45,60,70', model='resnet50q', optimizer='sgd', pretrain=None, print_freq=20, results_dir='/content/mydrive/any-precision-nets/results/exp_imagenet_resnet50q_1_0.3_lr_20200330_141640', resume='resnet50q_any_recursive.pth.tar', start_epoch=0, train_split='train', weight_decay=1e-05, workers=16)
2020-03-30 14:16:45 - INFO - loaded resume checkpoint 'resnet50q_any_recursive.pth.tar'
2020-03-30 14:16:45 - INFO - number of parameters: 25769512
